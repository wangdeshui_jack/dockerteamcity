param(
    [Parameter(Mandatory=$true)]
    [string] $sa_password
    )

# start the service
Write-Verbose 'Starting SQL Server'
Start-Service MSSQL`$SQLEXPRESS

if ($sa_password -ne "_") {
    Write-Verbose 'Changing SA login credentials'
    $sqlcmd = "ALTER LOGIN sa with password='$sa_password'; ALTER LOGIN sa ENABLE;"
    Invoke-SqlCmd -Query $sqlcmd -ServerInstance ".\SQLEXPRESS" `
}`
# # deploy or upgrade the database:
$SqlPackagePath = 'C:\Program Files (x86)\Microsoft SQL Server\140\DAC\bin\SqlPackage.exe'
& $SqlPackagePath  `
    /a:Publish `
    /sf:TASDB.dacpac `
    /p:CommentOutSetVarDeclarations=false `
    /tsn:.\SQLEXPRESS `
    /tdn:TASDB `
    /tu:sa `
    /tp:$sa_password `
